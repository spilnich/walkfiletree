package com.javarush.task.task31.task3101;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/*
Проход по дереву файлов
*/
public class Solution {
    public static void main(String[] args) throws IOException {

        File file = new File(args[0]);
//        File file = new File("C:\\Users\\spilnich\\Desktop\\test");
//        File resultFileAbsolutePath = new File("C:\\Users\\spilnich\\Desktop\\1allFilesContent.txt\\j.txt");
        File resultFileAbsolutePath = new File(args[1]);
        File newFileName = new File(resultFileAbsolutePath.getParent() + "/allFilesContent.txt");
//        if (!FileUtils.isExist(newFileName)) {
            FileUtils.renameFile(resultFileAbsolutePath, newFileName);
//        }
        Set<File> fileNames = new TreeSet<>(Comparator.comparing(File::getName));

        try (FileWriter writer = new FileWriter(newFileName, true)) {


            Files.walkFileTree(Paths.get(String.valueOf(file)), new SimpleFileVisitor<Path>() {


                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {


                    File fileTemp = new File(String.valueOf(file));

                    if (fileTemp.length() <= 50) {
                        fileNames.add(fileTemp);
                    }
                    return FileVisitResult.CONTINUE;
                }
            });

            for (File fileName : fileNames) {
//                System.out.println(fileName.getAbsolutePath());
                try (BufferedReader br = new BufferedReader(new FileReader(fileName));) {
                    while (br.ready()) {
                        String lines = br.readLine();
                        writer.write(lines);
                    }
                    writer.write("\n");

//                    List<String> files = Files.readAllLines(Paths.get(fileName.getAbsolutePath()), Charset.defaultCharset());
//                    for (String lines : files) {
//                        writer.write(lines);
//                    }
                }
            }
        }
    }
}
